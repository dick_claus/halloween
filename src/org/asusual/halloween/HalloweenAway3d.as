package org.asusual.halloween {
    import flash.display.StageDisplayState;
    import flash.ui.Keyboard;
    import flash.events.KeyboardEvent;
    import arsupport.away3d.ARAway3DContainer;
    import arsupport.away3d4.ARAway3D4Container;
    import arsupport.away3d4.Away3D4Lens;
    import arsupport.demo.away3d.Away3DWorld;

    import away3d.cameras.Camera3D;
    import away3d.containers.Scene3D;
    import away3d.containers.View3D;
    import away3d.debug.AwayStats;
    import away3d.textures.BitmapTexture;

    import ru.inspirit.asfeat.ASFEAT;
    import ru.inspirit.asfeat.IASFEAT;
    import ru.inspirit.asfeat.calibration.IntrinsicParameters;
    import ru.inspirit.asfeat.detect.ASFEATReference;
    import ru.inspirit.asfeat.event.ASFEATCalibrationEvent;
    import ru.inspirit.asfeat.event.ASFEATDetectionEvent;
    import ru.inspirit.asfeat.event.ASFEATLSHIndexEvent;

    import flash.display.BitmapData;
    import flash.display.Sprite;
    import flash.display.StageAlign;
    import flash.display.StageScaleMode;
    import flash.events.Event;
    import flash.filters.DropShadowFilter;
    import flash.geom.Matrix;
    import flash.geom.Vector3D;
    import flash.media.Camera;
    import flash.media.Video;
    import flash.text.TextField;
    import flash.text.TextFormat;
    import flash.utils.ByteArray;
	
	
	
	
	/**
	 * @author Eugene Zatepyakin
	 */
	[SWF(width='640',height='480',frameRate='25',backgroundColor='0xFFFFFF')]
	public final class HalloweenAway3d extends Sprite 
	{
		// tracking data file
		[Embed(source="../assets/marker1.ass", mimeType="application/octet-stream")]
		public static const DefinitionaData:Class;
        
        [Embed(source="../assets/marker2.ass", mimeType="application/octet-stream")]
        public static const DefinitionaData2:Class;
        
        [Embed(source="../assets/marker3.ass", mimeType="application/octet-stream")]
        public static const DefinitionaData3:Class;
		
		//asfeat variables
		private var asfeat:ASFEAT;
		private var asfeatLib:IASFEAT;
		private var intrinsic:IntrinsicParameters;
		private var maxTransformError:Number = 10 * 10 * 3;
		private var maxPointsToDetect:int = 300; // max point to allow on the screen
		private var maxReferenceObjects:int = 3; // max reference objects to be used
		
		//engine variables
		private var scene:Scene3D;
		private var camera:Camera3D;
		private var view:View3D;
		private var awayStats:AwayStats;
		
		
		// different visual objects
        public static var text:TextField;
        private var video:Video;
		private var cameraBuffer:BitmapData;
		private var backgroundTexture:BitmapTexture;
		private var buffer:BitmapData;
		private var cameraMatrix:Matrix;
		
		// 3d stuff
		private var asFeatLens:Away3D4Lens;
		private var model:ARAway3D4Container;
		
	
		// camera size
		public var camWidth:int = 1024;
        public var camHeight:int = 1024;
        public var srcWidth:int = 640;
        public var srcHeight : int = 480;
        private var model2 : ARAway3D4Container;
        private var model3 : ARAway3D4Container;
        
        private var models : Vector.<ARAway3D4Container>;
        
		
		public function HalloweenAway3d() 
		{
            
			asfeat = new ASFEAT(null);
			asfeat.addEventListener(Event.INIT, init); // wait before this event fired or it wont work
            
            mouseChildren = false;
            mouseEnabled = false;
		}
		
		private function init(e:Event = null):void
		{
            initCamera();
			initASFEAT();
			initEngine();
			initText();
			initObjects();
			initListeners();
		}
		
		private function initCamera():void
		{
			var camera:Camera = Camera.getCamera();
			camera.setMode(srcWidth, srcHeight, 25, true);
			
			video = new Video(camera.width, camera.height);
			video.attachCamera(camera);
			
			cameraMatrix = new Matrix(-camWidth/srcWidth, 0, 0, camHeight/srcHeight, camWidth);
			
			buffer = new BitmapData(srcWidth, srcHeight, false, 0x00);
			cameraBuffer = new BitmapData(camWidth, camHeight, false, 0x0);
		}
		
		private function initASFEAT():void
		{
			asfeat.removeEventListener( Event.INIT, init );
			asfeatLib = asfeat.lib;
            //asfeatLib = new ASFEATWrapper();
			
			// init our engine
            asfeatLib.init( srcWidth, srcHeight, maxPointsToDetect, maxReferenceObjects, maxTransformError, stage );
			
			// add reference object
			asfeatLib.addReferenceObject( ByteArray( new DefinitionaData ) );
            asfeatLib.addReferenceObject( ByteArray( new DefinitionaData2 ) );
            asfeatLib.addReferenceObject( ByteArray( new DefinitionaData3 ) );
			
			// ATTENTION 
			// use it if u want only one model to be detected
			// and available at single frame (better performance)
			asfeatLib.setSingleReferenceMode(false);
			
			// indexing reference data will result in huge
			// speed up during matching (see docs for more info)
			asfeatLib.indexReferenceData(20, 12);

			// u can repform geometric calibration
			// during detection/tracking (see onCalibDone method)
			//asfeatLib.startGeometricCalibration();
		}
		
		private function initEngine():void
		{
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
			intrinsic = asfeatLib.getIntrinsicParams();
			
			asFeatLens = new Away3D4Lens(intrinsic, srcWidth, srcHeight, 1.0);
			
			view = new View3D();
			view.camera.lens = asFeatLens;
			view.camera.position = new Vector3D();
			
			view.antiAlias = 4;
			backgroundTexture = new BitmapTexture(cameraBuffer);
			view.background = backgroundTexture;
			
			addChild(view);
			
			//awayStats = new AwayStats(view);
			//addChild(awayStats);
		}
		
		private function initText():void
		{
			// DEBUG TEXT FIELD
			text = new TextField();
			text.defaultTextFormat = new TextFormat("Verdana", 11, 0xFFFFFF);
			text.width = 300;
			text.height = 100;
			text.selectable = false;
			text.mouseEnabled = false;
			text.filters = [new DropShadowFilter(1, 45, 0x0, 1, 0, 0)];
			addChild(text);
		}
		
		private function initObjects():void
		{
			model = new Skull();
			view.scene.addChild(model);
            
            model2 = new Skeleton();
            view.scene.addChild(model2);
            
            model3 = new Pumpkin();
            view.scene.addChild(model3);
            
            models = Vector.<ARAway3D4Container>([
                model, model2, model3
                                                ]);
		}
		
		private function initListeners():void
		{
			asfeatLib.addListener(ASFEATDetectionEvent.DETECTED, onModelDetected);
			asfeatLib.addListener(ASFEATCalibrationEvent.COMPLETE, onCalibDone);
			//asfeatLib.addListener(ASFEATLSHIndexEvent.COMPLETE, onIndexComplete);
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
			onResize();
            stage.addEventListener(Event.RESIZE, onResize);
            stage.addEventListener(KeyboardEvent.KEY_DOWN, onSpaceDown);
        }

        private function onSpaceDown(event : KeyboardEvent) : void {
            if (event.keyCode == Keyboard.SPACE) {
                trace("space");
                if  (stage.displayState != StageDisplayState.FULL_SCREEN) {
                    stage.displayState = StageDisplayState.FULL_SCREEN;
                }
            }
        }
		
		private function onEnterFrame(e:Event = null):void
		{	
			//draw video stream to detection buffer & run detection
			buffer.draw(video, null, null, null, null, true);
			asfeatLib.detect(buffer);
			
			// draw detection buffer to camera buffer and flip result
			cameraBuffer.draw(buffer, cameraMatrix);
			
			//manually invalidate background texture
			backgroundTexture.invalidateContent();
			
			// call it each frame so if lost will accur
			// more then 5 frames with no detected/tracked event
			// it will be erased from the screen
			model.lost();
            model2.lost();
            model3.lost();
			text.text = '';
			
			view.render();
		}
		
		private function onModelDetected(e:ASFEATDetectionEvent):void
		{
			var refList:Vector.<ASFEATReference> = e.detectedReferences;
			var ref:ASFEATReference;
			var n:int = e.detectedReferencesCount;
			var state:String;
			
			for(var i:int = 0; i < n; ++i) {
				ref = refList[i];
				state = ref.detectType;  
                //trace(ref.id);              
				models[ref.id].visible = true;
				models[ref.id].setTransform( ref.rotationMatrix, ref.translationVector, ref.poseError, true );
                
				text.text = state;
				
				if(state == '_detect')
					text.appendText( '\nmatched: ' + ref.matchedPointsCount );
				
				text.appendText( '\nfound id: ' + ref.id );
			}
			
			text.appendText( '\ncalib fx/fy: ' + [intrinsic.fx, intrinsic.fy] );
		}
		
		private function onCalibDone(e:ASFEATCalibrationEvent):void
		{
			var val:Number = (e.fx + e.fy) * 0.5;
			
			intrinsic.update(val, val, intrinsic.cx, intrinsic.cy);

			asfeatLib.updateIntrinsicParams();
			asFeatLens.updateIntrinsic(srcWidth, srcHeight, 1.0);
			
			text.appendText( '\ncalib fx/fy: ' + [intrinsic.fx, intrinsic.fy] );
		}
		
		private function onIndexComplete(e:ASFEATLSHIndexEvent):void 
		{
			//trace(e.indexInfo);
		}

		
		/**
		 * stage listener for resize events
		 */
		private function onResize(event:Event = null):void
		{
			view.width = stage.stageWidth;
			view.height = stage.stageHeight;
			text.y = stage.stageHeight - text.height;
            
			//awayStats.x = stage.stageWidth - awayStats.width;
		}
		
	}

}