package org.asusual.halloween {
    import flash.geom.Vector3D;
    import away3d.library.assets.AssetType;
    import away3d.loaders.misc.AssetLoaderContext;
    import flash.utils.ByteArray;
    import away3d.events.AssetEvent;
    import away3d.materials.TextureMaterial;
    import away3d.textures.BitmapTexture;
    import away3d.loaders.parsers.OBJParser;
    import away3d.library.AssetLibrary;
    import away3d.entities.Mesh;
    import arsupport.away3d4.ARAway3D4Container;

    /**
     * @author Dmitry.Bezverkhiy
     */
    public class Skull extends ARAway3D4Container {
        
        // Model and texture provided by Daniel F. R Gordillo 
        // downoladed from blendswap.com under CC-BY license 
        
        [Embed(source="../../../../assets/models/textures/skull_texture.jpg")] 
        private static var SkullTexture:Class;
        
        [Embed(source="../../../../assets/models/craneo.obj", mimeType="application/octet-stream")] 
        private static var SkullMesh:Class;
        
        private var _mesh:Mesh;
        private var skullTexture : BitmapTexture;
        private var skullMaterial : TextureMaterial;
        
        public function Skull() {
            super();
            initObjects();
        }
        
        private function initObjects():void
        {           
            skullTexture = new BitmapTexture(new SkullTexture().bitmapData);            
            skullMaterial = new TextureMaterial(skullTexture);
            
            
            var _parserobj:OBJParser = new OBJParser(40);
            //AssetLibrary.enableParser(OBJParser);           
            
            AssetLibrary.addEventListener(AssetEvent.ASSET_COMPLETE, onAssetRetrieved);
            AssetLibrary.loadData(ByteArray(new SkullMesh()), null, "skull", _parserobj);
        }
        
        private function onAssetRetrieved(event : AssetEvent) : void
        {
            if (event.asset.assetNamespace == "skull" && event.asset.assetType == AssetType.MESH) 
            {
                _mesh = Mesh(event.asset);
                _mesh.transform.appendScale( -1, -1, 1);
                _mesh.transform.appendRotation( -90, Vector3D.X_AXIS );

                addChild(_mesh);
                
                _mesh.material = skullMaterial;
                
            }
        }
    }
}
