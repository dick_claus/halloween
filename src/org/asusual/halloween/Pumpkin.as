/**
 * Created by FDT
 * @author Dmitry Bezverkhiy
 * Date: Oct 24, 2012
 * Time: 4:09:39 PM
 */
package org.asusual.halloween {
    import flash.utils.setTimeout;
    import away3d.core.base.SubMesh;
    import flash.geom.Vector3D;
    import away3d.library.assets.AssetType;
    import away3d.loaders.misc.AssetLoaderContext;
    import flash.utils.ByteArray;
    import away3d.events.AssetEvent;
    import away3d.library.AssetLibrary;
    import away3d.loaders.parsers.OBJParser;
    import away3d.textures.BitmapTexture;
    import away3d.entities.Mesh;
    import away3d.materials.TextureMaterial;
    import arsupport.away3d4.ARAway3D4Container;

    public class Pumpkin extends ARAway3D4Container {
        
        [Embed(source="../../../../assets/models/textures/nose.png")] 
        private static var Green:Class;
        
        [Embed(source="../../../../assets/models/textures/pump.png")] 
        private static var Orange:Class;
        
        [Embed(source="../../../../assets/models/WitchStuff.obj", mimeType="application/octet-stream")] 
        private static var Charmesh:Class;
        
        private var _mesh:Mesh;
        private var greenTexture:BitmapTexture;
        private var orangeTexture : BitmapTexture;
        private var _material:TextureMaterial;
        private var noseMaterial : TextureMaterial;
        private var pumpMaterial : TextureMaterial;
        
        // Model and texture provided by Paulo Martins 
        // downoladed from blendswap.com under CC-BY license
                
        public function Pumpkin() {
            super();
            initObjects();
        }
        
        private function initObjects():void
        {           
            greenTexture = new BitmapTexture(new Green().bitmapData);
            orangeTexture = new BitmapTexture(new Orange().bitmapData);
            noseMaterial = new TextureMaterial(greenTexture);
            pumpMaterial = new TextureMaterial(orangeTexture);
            
            var _parserobj:OBJParser = new OBJParser(180);
            //AssetLibrary.enableParser(OBJParser);           
            
            AssetLibrary.addEventListener(AssetEvent.ASSET_COMPLETE, onAssetRetrieved);
            AssetLibrary.loadData(ByteArray(new Charmesh), null, "pumpkin", _parserobj);
        }
        
        private function onAssetRetrieved(event : AssetEvent) : void
        {
            if (event.asset.assetNamespace == "pumpkin"  && event.asset.assetType == AssetType.MESH) 
            {
                trace(event.asset.assetNamespace);
                _mesh = Mesh(event.asset);
                _mesh.transform.appendScale( -1, -1, 1);
                _mesh.transform.appendRotation( -90, Vector3D.X_AXIS );

                addChild(_mesh);
                
                _mesh.material = pumpMaterial;
                
                
                
                
                //Mesh(AssetLibrary.getAsset("pumpkin-face_face")).material = pumpMaterial;
            } 
        }
    }
}
