/**
 * Created by FDT
 * @author Dmitry Bezverkhiy
 * Date: Oct 24, 2012
 * Time: 3:32:39 PM
 */
package org.asusual.halloween {
    import away3d.core.base.SubMesh;
    import flash.geom.Vector3D;
    import away3d.library.assets.AssetType;
    import away3d.loaders.misc.AssetLoaderContext;
    import flash.utils.ByteArray;
    import away3d.events.AssetEvent;
    import away3d.library.AssetLibrary;
    import away3d.loaders.parsers.OBJParser;
    import away3d.textures.BitmapTexture;
    import away3d.entities.Mesh;
    import away3d.materials.TextureMaterial;
    import arsupport.away3d4.ARAway3D4Container;

    public class Skeleton extends ARAway3D4Container {
        
        [Embed(source="../../../../assets/models/textures/skeleton.png")] 
        private static var Charmap:Class;
        
        [Embed(source="../../../../assets/models/skeleton.obj", mimeType="application/octet-stream")] 
        private static var Charmesh:Class;
        
        private var _mesh:Mesh;
        private var _texture:BitmapTexture;
        private var _material:TextureMaterial;
        
        public function Skeleton() {
            super();
            initObjects();
        }
        
        private function initObjects():void
        {           
            _texture = new BitmapTexture(new Charmap().bitmapData);
            _material = new TextureMaterial(_texture);
            
            var _parserobj:OBJParser = new OBJParser(50);
            //AssetLibrary.enableParser(OBJParser);           
            
            AssetLibrary.addEventListener(AssetEvent.ASSET_COMPLETE, onAssetRetrieved);
            AssetLibrary.loadData(ByteArray(new Charmesh), null, "skeleton", _parserobj);
        }
        
        private function onAssetRetrieved(event : AssetEvent) : void
        {
            if (event.asset.assetNamespace == "skeleton"  && event.asset.assetType == AssetType.MESH) 
            {
                _mesh = Mesh(event.asset);
                _mesh.transform.appendScale( -1, -1, 1);
                _mesh.transform.appendRotation( -90, Vector3D.X_AXIS );

                addChild(_mesh);
                
                for each (var subMesh : SubMesh in _mesh.subMeshes) {
                    subMesh.material = _material;
                }
                                
            }
        }
    }
}
