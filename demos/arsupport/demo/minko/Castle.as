package arsupport.demo.minko 
{
    import aerys.minko.render.effect.Effect;
    import aerys.minko.render.resource.texture.TextureResource;
    import aerys.minko.render.shader.compiler.graph.nodes.AbstractNode;
    import aerys.minko.scene.node.AbstractSceneNode;
	import aerys.minko.scene.node.Group;
    import aerys.minko.scene.node.ISceneNode;
    import aerys.minko.scene.node.mesh.Mesh;
    import aerys.minko.type.data.DataProvider;
    import aerys.minko.type.loader.ILoader;
    import aerys.minko.type.loader.parser.ParserOptions;
    import aerys.minko.type.loader.SceneLoader;
    import aerys.minko.type.loader.TextureLoader;
    import aerys.minko.type.math.Vector4;
    import aerys.minko.type.parser.collada.ColladaParser;
    import arsupport.minko.MinkoCaptureTexture;
    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.geom.Vector3D;
    import flash.utils.Dictionary;
    import flash.utils.setTimeout;
	
	/**
     * ...
     * @author Eugene Zatepyakin
     */
    public final class Castle extends Group 
    {
        [Embed(source = "../../../../assets/iskwen/bleh.dae", mimeType = "application/octet-stream")]
        protected static const DAE:Class;
        
        // textures
        [Embed(source = "../../../../assets/iskwen/91070007.jpg")] protected static const ass_91070007:Class;
        [Embed(source = "../../../../assets/iskwen/91070009_pattern.jpg")] protected static const ass_91070009_pattern:Class;
        [Embed(source = "../../../../assets/iskwen/91070011.jpg")] protected static const ass_91070011:Class;
        [Embed(source = "../../../../assets/iskwen/91070012.jpg")] protected static const ass_91070012:Class;
        [Embed(source = "../../../../assets/iskwen/grnd05.jpg")] protected static const ass_grnd05:Class;
        [Embed(source = "../../../../assets/iskwen/OLDMETAL.JPG")] protected static const ass_OLDMETAL:Class;
        [Embed(source = "../../../../assets/iskwen/OLDWOOD.JPG")] protected static const ass_OLDWOOD:Class;
        [Embed(source = "../../../../assets/iskwen/Thermal & Moisture.Roofing & Siding Panels.Wood.Horizontal.Cedar.jpg")] protected static const ass_roof:Class;
        
        protected static var texture_dict:Dictionary = new Dictionary();
        {
            texture_dict["91070007.jpg"] = ass_91070007;
            texture_dict["91070009_pattern.jpg"] = ass_91070009_pattern;
            texture_dict["91070011.jpg"] = ass_91070011;
            texture_dict["91070012.jpg"] = ass_91070012;
            texture_dict["grnd05b.jpg"] = ass_grnd05;
            texture_dict["br008.jpg"] = ass_grnd05;
            texture_dict["oldmetal.jpg"] = ass_OLDMETAL;
            texture_dict["oldwood.jpg"] = ass_OLDWOOD;
            texture_dict["thermal & moisture.roofing & siding panels.wood.horizontal.cedar.jpg"] = ass_roof;
        }
        
        public var dae_model:SceneLoader;
        protected var _lightTexture:MinkoCaptureTexture;
        protected var _meshes:Vector.<ISceneNode>;
        
        public function Castle() 
        {
            var effect:Effect = new Effect(new LightMapShader);
            
            var options:ParserOptions = new ParserOptions();

			options.parser = ColladaParser;
			options.loadDependencies = true;
			options.mipmapTextures = true;
            options.dependencyLoaderClosure	= loadDependency;
            options.effect = effect;
            
            dae_model = new SceneLoader(options);
            dae_model.complete.add(loaderCompleteHandler);
            dae_model.loadClass(DAE);
        }
        
        public function setupLightMap(bmp:BitmapData):void
        {
            _lightTexture.setContentFromBitmapData(bmp, false);
        }
        public function updateLightMap():void
        {
            _lightTexture.update = true;
        }
        public function setupCameraBias(r:Number = 0, g:Number = 0, b:Number = 0):void
        {
            for each (var mesh : ISceneNode in _meshes)
            {   
                Mesh(mesh).properties.setProperty("cameraBias", new Vector4(r, g, b));
            }
        }
        
        private function loaderCompleteHandler(loader	: ILoader,
											   scene	: ISceneNode) : void
		{            
            _lightTexture = new MinkoCaptureTexture(128, 128);
            
            _meshes = scene is Group ? Group(scene).getDescendantsByType(Mesh) : new <ISceneNode>[scene];
            for each (var mesh : ISceneNode in _meshes)
            {   
                Mesh(mesh).properties.setProperty("lightMap", _lightTexture);
                Mesh(mesh).properties.setProperty("cameraBias", new Vector4);
            }
            
            scene.transform.prependUniformScale(80)
                                .prependRotation( -Math.PI / 2, Vector4.X_AXIS);
            addChild(scene);
		}
        
        public var _paths:Array = [];
        private function loadDependency(dependencyPath	: String,
										isTexture		: Boolean,
										options			: ParserOptions) : ILoader
		{
            //C:/Users/Sam/Desktop/Eugene/Bleh/91070012.jpg
            var path_arr:Array = dependencyPath.split("/");
            var file_name:String = path_arr[path_arr.length - 1];
            var file_name_l:String = file_name.toLowerCase();
            
            //throw new Error('path: ' + file_name);
            _paths.push(file_name);
            
            var loader : ILoader = new TextureLoader(options.mipmapTextures);
            try
            {
                if (file_name_l.indexOf("bump") > -1)
                {
                    loader.loadClass(texture_dict["91070007.jpg"]);
                } else {
                    if (file_name_l.indexOf("wood.horizontal.cedar") > -1)
                    {
                        loader.loadClass(texture_dict["thermal & moisture.roofing & siding panels.wood.horizontal.cedar.jpg"]);
                    }
                    else loader.loadClass(texture_dict[file_name_l]);
                }
            } catch (e:Error)
            {
                throw new Error('error texture: ' + file_name_l);
            }

			return loader;
		}
        
        public var surfNormal:Vector3D = new Vector3D();
        protected var _surfNormal4:Vector4 = new Vector4();
        public function getSurfaceNormal():Vector3D
        {
            //var dt:Vector.<Number> = this.transform.matrix3D.rawData;

            //surfNormal.x = dt[2];
            //surfNormal.y = dt[6];
            //surfNormal.z = dt[10];
			
            _surfNormal4.x = 0;
            _surfNormal4.y = 0;
            _surfNormal4.z = -1;
            _surfNormal4 = this.transform.deltaTransformVector(_surfNormal4);

            _surfNormal4.normalize();
            
            surfNormal.x = _surfNormal4.x;
            surfNormal.y = _surfNormal4.y;
            surfNormal.z = _surfNormal4.z;

            return surfNormal;
        }
        
    }

}